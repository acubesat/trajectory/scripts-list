# dramakit

A collection of algorithms for DRAMA's python interface, pyDRAMA.

## Contents

* `altitudeSolver.py` : Calculates the altitude for an SSO orbit sallite that has a given lifetime using automated simulation runs to scan the parameter space.

## Dependencies

Requirements for dramakit include `numpy`, `pandas`, `math` and `random`.