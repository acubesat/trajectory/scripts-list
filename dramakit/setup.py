import setuptools

setuptools.setup(
    name = "dramakit",
    version = "1.5",
    author = ("github.com/theorydas", "gitlab.com/iPhanes")
)