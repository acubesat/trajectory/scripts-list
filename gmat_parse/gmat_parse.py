from datetime import datetime as dt
from pathlib import Path
from statistics import mean
from string import whitespace
from typing import Callable, List, Tuple, Type, Union


def parse_gmat_file(filename: Path) -> List[str]:
    with filename.open(mode="r") as fid:
        content = fid.read().splitlines()
    return content


def sanitize_data(
    gmat_file_type: str,
    column_data_type: Type[Union[int, float, str]],
    column_data: List[Union[int, float, str]],
) -> List[Union[int, float, str]]:
    def _typecast(
        _type: Callable[[], Union[int, float]], lst: List[Union[int, float]]
    ) -> List[Union[int, float]]:
        return list(map(_type, lst))

    def _sanitize_type_entries(lst: List[str]):
        contains_whitespace = lambda s: any([c in s for c in whitespace])
        for index, type_entry in enumerate(lst):
            if contains_whitespace(type_entry):
                lst[index] = "Umbra"

    def _sanitize_time_entries(lst: List[str]):
        for index, time_entry in enumerate(lst):
            tmp = time_entry.split(".")
            tmp[1] = tmp[1].zfill(6)
            tmp[1] = ":" + tmp[1]
            lst[index] = "".join(tmp)

    if not column_data_type is str:
        column_data = _typecast(column_data_type, column_data)
    else:
        if gmat_file_type == "eclipse_time":
            _sanitize_type_entries(column_data)
        elif gmat_file_type == "comm_window":
            _sanitize_time_entries(column_data)

    return column_data


def parse_column_data(
    gmat_file: List[str],
    gmat_file_type: str,
    column_structure: List[Union[int, int, Union[Type[int], Type[float], Type[str]]]],
    special_line_counts: Tuple[int],
) -> List[Union[int, float, str]]:

    not_pass_line_count, header_line_count = special_line_counts
    NUM_OF_PASSES = len(gmat_file) - not_pass_line_count

    COLUMN_FIRST_CHAR_INDEX, COLUMN_CHAR_COUNT, COLUMN_DATA_TYPE = column_structure

    column_data = []
    for gmat_row_entry in range(header_line_count, header_line_count + NUM_OF_PASSES):
        column_data.append(
            gmat_file[gmat_row_entry][
                COLUMN_FIRST_CHAR_INDEX : COLUMN_FIRST_CHAR_INDEX + COLUMN_CHAR_COUNT
            ]
        )

    column_data = sanitize_data(gmat_file_type, COLUMN_DATA_TYPE, column_data)
    return column_data


def calculate_stats(
    durations_lst: List[float], round_precision: int = 3
) -> Tuple[float]:
    mean_duration = round(mean(durations_lst), round_precision)
    max_duration = round(max(durations_lst), round_precision)
    min_duration = round(min(durations_lst), round_precision)
    return mean_duration, max_duration, min_duration


def calculate_eclipse_percentage(
    orbital_lifetime: float, durations_lst: List[float]
) -> float:
    return (orbital_lifetime * 12 * 3600) / sum(durations_lst)


def calculate_time_diff_between_passes(
    start_times: List[str], stop_times: List[str]
) -> Tuple[float]:
    STRPTIME_FORMAT = "%d %b %Y %H:%M:%S:%f"
    for time_string_index in range(len(start_times)):
        start_times[time_string_index] = dt.strptime(
            start_times[time_string_index], STRPTIME_FORMAT
        )
        stop_times[time_string_index] = dt.strptime(
            stop_times[time_string_index], STRPTIME_FORMAT
        )

    time_diffs = []
    for i in range(1, len(start_times)):
        diff = start_times[i] - stop_times[i - 1]
        time_diffs.append(diff.total_seconds())

    return time_diffs


def main():
    PATH_TO_FILES = Path(Path(__file__).parent / "data")
    PATH_TO_ECLIPSE_TIME_FILES = PATH_TO_FILES / "eclipse_times"
    PATH_TO_COMM_WINDOWS_FILES = PATH_TO_FILES / "comm_windows"

    COLUMN_STRUCTURES = {
        "duration": [58, 12, float],
        "event_number": [102, 1, int],
        "start_time": [0, 24, str],
        "stop_time": [28, 24, str],
        "total_duration": [116, 12, float],
        "type": [90, 8, str],
    }

    SPECIAL_LINE_COUNTS = {"eclipse_time": (10, 3), "comm_window": (9, 4)}

    def _process_eclipse_times(filename: Path) -> Tuple[Union[int, float, str]]:
        ORBITAL_LIFETIME = 12

        content = parse_gmat_file(filename)

        durations = parse_column_data(
            content,
            "eclipse_time",
            COLUMN_STRUCTURES["duration"],
            SPECIAL_LINE_COUNTS["eclipse_time"],
        )
        total_durations = parse_column_data(
            content,
            "eclipse_time",
            COLUMN_STRUCTURES["total_duration"],
            SPECIAL_LINE_COUNTS["eclipse_time"],
        )
        types = parse_column_data(
            content,
            "eclipse_time",
            COLUMN_STRUCTURES["type"],
            SPECIAL_LINE_COUNTS["eclipse_time"],
        )
        event_numbers = parse_column_data(
            content,
            "eclipse_time",
            COLUMN_STRUCTURES["event_number"],
            SPECIAL_LINE_COUNTS["eclipse_time"],
        )

        penumbra_durations = []
        umbra_durations = []
        for index, _type in enumerate(types):
            if _type.startswith("Penumbra"):
                penumbra_durations.append(durations[index])
            elif _type.startswith("Umbra"):
                umbra_durations.append(durations[index])

        total_duration_stats = calculate_stats(total_durations)
        penumbra_stats = calculate_stats(penumbra_durations)
        umbra_stats = calculate_stats(umbra_durations)

        eclipse_percent = calculate_eclipse_percentage(
            ORBITAL_LIFETIME, total_durations
        )

        return (
            durations,
            total_durations,
            types,
            event_numbers,
            total_duration_stats,
            penumbra_stats,
            umbra_stats,
            eclipse_percent,
        )

    def _process_comms_windows(filename: Path) -> Tuple[float]:
        content = parse_gmat_file(filename)
        durations = parse_column_data(
            content,
            "comm_window",
            COLUMN_STRUCTURES["duration"],
            SPECIAL_LINE_COUNTS["comm_window"],
        )
        start_times = parse_column_data(
            content,
            "comm_window",
            COLUMN_STRUCTURES["start_time"],
            SPECIAL_LINE_COUNTS["comm_window"],
        )
        stop_times = parse_column_data(
            content,
            "comm_window",
            COLUMN_STRUCTURES["stop_time"],
            SPECIAL_LINE_COUNTS["comm_window"],
        )

        duration_stats = calculate_stats(durations)
        comm_window_durations = calculate_time_diff_between_passes(
            start_times, stop_times
        )
        comm_window_stats = calculate_stats(comm_window_durations)

        return (
            durations,
            start_times,
            stop_times,
            duration_stats,
            comm_window_durations,
            comm_window_stats,
        )

    def _eclipse_times_results():
        for txt_filename in PATH_TO_ECLIPSE_TIME_FILES.rglob("*.txt"):
            stats = _process_eclipse_times(txt_filename)[4:]
            print(
                f"""
                File: {txt_filename.name}
                ------------
                Duration stats (Mean, Max, Min):
                Total duration:
                {stats[0]}
                Penumbra duration:
                {stats[1]}
                Umbra duration:
                {stats[2]}"""
            )

    def _comm_windows_results():
        for txt_filename in PATH_TO_COMM_WINDOWS_FILES.rglob("*.txt"):
            stats = _process_comms_windows(txt_filename)[3::2]
            print(
                f"""
                File: {txt_filename.name}
                ------------
                Duration stats (Mean, Max, Min):
                Duration:
                {stats[0]}
                Comm window duration:
                {stats[1]}
                """
            )

    _eclipse_times_results()
    _comm_windows_results()


main()
