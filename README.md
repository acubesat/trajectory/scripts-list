An algorithm library for the various needs and programs of Trajectory.

## Contents

* `dramakit` : DRAMA utilities and automations.
* `gmat_parse` : GMAT output formatting.